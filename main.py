#! /usr/bin/python3
import subprocess
import re
from datetime import datetime
from subprocess import check_output
from nicegui import ui

## CONSTANTS ##
GET_THROTTLED_CMD = 'vcgencmd get_throttled'
MESSAGES = {
    0: 'Under-voltage!',
    1: 'ARM frequency capped!',
    2: 'Currently throttled!',
    3: 'Soft temperature limit active',
    16: 'Under-voltage has occurred since last reboot.',
    17: 'Throttling has occurred since last reboot.',
    18: 'ARM frequency capped has occurred since last reboot.',
    19: 'Soft temperature limit has occurred'
}

## SET FOCUS ##
def focus_cmd(value):
    subprocess.run(['v4l2-ctl', '-d', '/dev/v4l-subdev1', '--set-ctrl', 'focus_absolute=' + str(value)])

def temp_cmd():
    global temp
    temp = subprocess.check_output(['vcgencmd', 'measure_temp'])
    temp_formatted = re.sub('[^\d\.]', '', str(temp))
    temperature.set_text(f'Temperature: {temp_formatted}°C')

def throttle_cmd():
    #throttle_check = subprocess.run(['vcgencmd' 'get_throttled'])
    #status.set_text(str(throttle_check))
    throttled_output = subprocess.check_output(GET_THROTTLED_CMD, shell=True)
    throttled_binary = bin(int(throttled_output.split(b'=')[1], 0))
    
    warnings = 0

    for position, message in MESSAGES.items():
        # Check for the binary digits to be "on" for each warning message
        if len(throttled_binary) > position and throttled_binary[0 - position - 1] == '1':
            #print(message)
            current_status.set_text(message)
            warnings += 1

    if warnings == 0:
        total_status.set_text("Gucci Mango. No throttling since last reboot.")
    else:
        total_status.set_text("Throttling has occured since last reboot.")

def restart_ndi():
    subprocess.run(['sudo', 'service', 'ndicam', 'restart'])

def restart_ui():
    subprocess.run(['sudo', 'service', 'focus-gui', 'restart'])

## FOCUS ##

focus_slider = ui.slider(min=0, max=4095, value=1000, on_change = lambda e: focus_cmd(e.value) ).props('label')
ui.label().bind_text_from(focus_slider, 'value')

with ui.row():

    ## TEMPERATURE ##
    ui.timer(interval=5, callback=temp_cmd)
    #temperature_button = ui.button('Get Temperature', on_click= temp_cmd )
    temperature = ui.label('Temperature Not Yet Checked')

    ## THROTTLING ##
    ui.timer(interval=5, callback=throttle_cmd)
    current_status = ui.label("Status Not Checked")
    total_status = ui.label("Status Not Checked")

with ui.row():
    ## NDI SERVICE ##
    ui.button('Restart NDI Service', on_click= restart_ndi )

    ## RESTART UI ##
    ui.button('Restart UI (Page Will BRB)', on_click= restart_ui )


ui.run(dark=True, title="SPOTLAB Camera Control Interface")